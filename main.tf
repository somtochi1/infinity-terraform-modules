provider "azurerm" {
  version = "~>2.0.0"
  features {}
}

resource "azurerm_resource_group" "default" {
  name     = "infinity-css-rg"
  location = "West Europe"
}

resource "azurerm_kubernetes_cluster" "default" {
  name                = "infinity-css-aks"
  location            = azurerm_resource_group.default.location
  resource_group_name = azurerm_resource_group.default.name
  dns_prefix          = "infinity-css"

  default_node_pool {
    name       = "default"
    node_count = 3
    vm_size    = "Standard_A2_v2"
  }

  service_principal {
    client_id     = var.clientId
    client_secret = var.clientSecret
  }

  role_based_access_control {
    enabled = true
  }

  tags = {
    environment = "preproduction"
  }
}
