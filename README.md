## Terraform modules for the Infinity CCS project

## Pre-requisities
- Install [Azure CLI](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli)
- Login to the CLI:  `az login`

## Next Steps
Initalize Terraform in workspace.

```shell
$ terraform init

Initializing provider plugins...
- Checking for available provider plugins...
- Downloading plugin for provider "azurerm" (hashicorp/azurerm) 1.44.0...

Terraform has been successfully initialized!
```


Provision Azure ACR.

```shell
$ terraform apply

# Output truncated...

Plan: 2 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

# Output truncated...

Apply complete! Resources: 2 added, 0 changed, 0 destroyed.
```

## Requirements

| Name | Version |
|------|---------|
| azurerm | ~>2.0.0 |

## Providers

| Name | Version |
|------|---------|
| azurerm | ~>2.0.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| clientId | n/a | `any` | n/a | yes |
| clientSecret | n/a | `any` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| kubeconfig | n/a |
