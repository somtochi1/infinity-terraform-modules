output "kubeconfig" {
  value = azurerm_kubernetes_cluster.default.kube_config_raw
}
